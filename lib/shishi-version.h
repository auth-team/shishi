/* shishi-version.h --- Header file with Shishi version.              -*- c -*-
 * Copyright (C) 2002-2022 Simon Josefsson
 *
 * This file is part of Shishi.
 *
 * Shishi is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shishi is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shishi; if not, see http://www.gnu.org/licenses or write
 * to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef SHISHI_VERSION_H
# define SHISHI_VERSION_H

/**
 * SECTION:shishi-version
 * @title: shishi-version.h
 * @short_description: version symbols
 *
 * The shishi-version.h file contains version symbols.  It should
 * not be included directly, only via shishi.h.
 */
  /**
   * SHISHI_VERSION
   *
   * Pre-processor symbol with a string that describe the header file
   * version number.  Used together with shishi_check_version() to
   * verify header file and run-time library consistency.
   */
# define SHISHI_VERSION "1.0.3"

  /**
   * SHISHI_VERSION_MAJOR
   *
   * Pre-processor symbol with a decimal value that describe the major
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 1.
   *
   * Since: 1.0.3
   */
# define SHISHI_VERSION_MAJOR 1

  /**
   * SHISHI_VERSION_MINOR
   *
   * Pre-processor symbol with a decimal value that describe the minor
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 2.
   *
   * Since: 1.0.3
   */
# define SHISHI_VERSION_MINOR 0

  /**
   * SHISHI_VERSION_PATCH
   *
   * Pre-processor symbol with a decimal value that describe the patch
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 3.
   *
   * Since: 1.0.3
   */
# define SHISHI_VERSION_PATCH 3

  /**
   * SHISHI_VERSION_NUMBER
   *
   * Pre-processor symbol with a hexadecimal value describing the
   * header file version number.  For example, when the header version
   * is 1.2.3 this symbol will have the value 0x010203.
   *
   * Since: 1.0.3
   */
# define SHISHI_VERSION_NUMBER 0x010003

#endif /* SHISHI_VERSION_H */
