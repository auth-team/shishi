# Copyright (C) 2006-2022 Simon Josefsson.
#
# This file is part of Shishi.
#
# Shishi is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Shishi is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shishi; if not, see http://www.gnu.org/licenses or write
# to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.

manual_title = Kerberos for the GNU system

old_NEWS_hash = dff4e9e65254c85bf2485d1a9c3a8378

bootstrap-tools = autoconf,automake,libtoolize,gnulib,makeinfo,help2man,gengetopt,gtkdocize,tar,gzip,bison

local-checks-to-skip = \
	sc_error_message_uppercase	\
	sc_prohibit_atoi_atof		\
	sc_prohibit_gnu_make_extensions \
	sc_prohibit_have_config_h	\
	sc_prohibit_strcmp		\
	sc_require_config_h		\
	sc_require_config_h_first	\

exclude_file_name_regexp--sc_bindtextdomain = ^examples/|extra/|lib/ccache.c|tests/utils.c
exclude_file_name_regexp--sc_prohibit_doubled_word = ^doc/shishi.texi|lib/
exclude_file_name_regexp--sc_prohibit_empty_lines_at_EOF = ^doc/components.dia|doc/components.png|tests/ccache1.bin|tests/keytab1.bin
exclude_file_name_regexp--sc_cast_of_argument_to_free = ^lib/keys.c|lib/tkts.c|extra/pam_shishi/pam_shishi.c
exclude_file_name_regexp--sc_program_name = ^examples/|extra/|lib/|tests/
exclude_file_name_regexp--sc_prohibit_cvs_keyword = ^extra/rsh-redone/
exclude_file_name_regexp--sc_prohibit_magic_number_exit = ^extra/rsh-redone/
exclude_file_name_regexp--sc_space_tab = ^doc/components.dia|extra/rsh-redone/
exclude_file_name_regexp--sc_trailing_blank = ^doc/components.png|doc/shishi.texi|extra/fetchmail.diff|extra/rsh-redone/
exclude_file_name_regexp--sc_two_space_separator_in_usage = ^doc/shishi.texi
exclude_file_name_regexp--sc_unmarked_diagnostics = ^extra/rsh-redone/|src/shisa.c|src/shishi.c|src/shishid.c
exclude_file_name_regexp--sc_useless_cpp_parens = ^extra/rsh-redone/
exclude_file_name_regexp--sc_prohibit_strncpy =^lib/error.c|src/shishid.c

VC_LIST_ALWAYS_EXCLUDE_REGEX = ^GNUmakefile|maint.mk|gtk-doc.make|build-aux/|doc/keytab.txt|doc/fdl-1.3.texi|doc/gdoc|doc/parse-datetime.texi|doc/specifications/.*|extra/.*\.[1ch]|m4/pkg.m4|po/.*.po.in|lib/gl/.*|src/gl/.*$$

update-copyright-env = UPDATE_COPYRIGHT_HOLDER="Simon Josefsson" UPDATE_COPYRIGHT_USE_INTERVALS=2

review-diff:
	git diff `git describe --abbrev=0`.. \
	| grep -v -e ^index -e '^diff --git' \
	| filterdiff -p 1 -x 'build-aux/*' -x 'gl/*' -x 'db/gl/*' -x 'src/gl/*' -x 'po/*' -x 'maint.mk' -x '.gitignore' -x '.x-sc*' -x ChangeLog -x GNUmakefile \
	| less
