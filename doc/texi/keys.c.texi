@subheading shishi_keys
@anchor{shishi_keys}
@deftypefun {int} {shishi_keys} (@w{Shishi * @var{handle}}, @w{Shishi_keys ** @var{keys}})
@var{handle}: shishi handle as allocated by @code{shishi_init()}.
@*@var{keys}: output pointer to newly allocated keys handle.

@strong{Description:} Get a new key set handle.

@strong{Return value:} Returns @code{SHISHI_OK} iff successful.
@end deftypefun

@subheading shishi_keys_done
@anchor{shishi_keys_done}
@deftypefun {void} {shishi_keys_done} (@w{Shishi_keys ** @var{keys}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.

@strong{Description:} Deallocates all resources associated with key set.  The key set
handle must not be used in calls to other shishi_keys_*() functions
after this.
@end deftypefun

@subheading shishi_keys_size
@anchor{shishi_keys_size}
@deftypefun {int} {shishi_keys_size} (@w{Shishi_keys * @var{keys}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.

@strong{Description:} Get size of key set.

@strong{Return value:} Returns number of keys stored in key set.
@end deftypefun

@subheading shishi_keys_nth
@anchor{shishi_keys_nth}
@deftypefun {const Shishi_key *} {shishi_keys_nth} (@w{Shishi_keys * @var{keys}}, @w{int @var{keyno}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.
@*@var{keyno}: integer indicating requested key in key set.

@strong{Get the n:} th ticket in key set.

@strong{Return value:} Returns a key handle to the keyno:th key in the key
set, or NULL if @var{keys} is invalid or @var{keyno} is out of bounds.  The
first key is @var{keyno} 0, the second key @var{keyno} 1, and so on.
@end deftypefun

@subheading shishi_keys_remove
@anchor{shishi_keys_remove}
@deftypefun {void} {shishi_keys_remove} (@w{Shishi_keys * @var{keys}}, @w{int @var{keyno}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.
@*@var{keyno}: key number of key in the set to remove.  The first
key is key number 0.

@strong{Description:} Remove a key, indexed by @var{keyno}, in given key set.
@end deftypefun

@subheading shishi_keys_add
@anchor{shishi_keys_add}
@deftypefun {int} {shishi_keys_add} (@w{Shishi_keys * @var{keys}}, @w{Shishi_key * @var{key}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.
@*@var{key}: key to be added to key set.

@strong{Description:} Add a key to the key set.  A deep copy of the key is stored, so
changing @var{key}, or deallocating it, will not modify the value stored
in the key set.

@strong{Return value:} Returns @code{SHISHI_OK} iff successful.
@end deftypefun

@subheading shishi_keys_print
@anchor{shishi_keys_print}
@deftypefun {int} {shishi_keys_print} (@w{Shishi_keys * @var{keys}}, @w{FILE * @var{fh}})
@var{keys}: key set to print.
@*@var{fh}: file handle, open for writing, to print keys to.

@strong{Description:} Print all keys in set using shishi_key_print.

@strong{Returns:} Returns @code{SHISHI_OK} on success.
@end deftypefun

@subheading shishi_keys_to_file
@anchor{shishi_keys_to_file}
@deftypefun {int} {shishi_keys_to_file} (@w{Shishi * @var{handle}}, @w{const char * @var{filename}}, @w{Shishi_keys * @var{keys}})
@var{handle}: shishi handle as allocated by @code{shishi_init()}.
@*@var{filename}: filename to append key to.
@*@var{keys}: set of keys to print.

@strong{Description:} Print an ASCII representation of a key structure to a file, for
each key in the key set.  The file is appended to if it exists.
See @code{shishi_key_print()} for the format of the output.

@strong{Return value:} Returns @code{SHISHI_OK} iff successful.
@end deftypefun

@subheading shishi_keys_from_file
@anchor{shishi_keys_from_file}
@deftypefun {int} {shishi_keys_from_file} (@w{Shishi_keys * @var{keys}}, @w{const char * @var{filename}})
@var{keys}: key set handle as allocated by @code{shishi_keys()}.
@*@var{filename}: filename to read keys from.

@strong{Description:} Read zero or more keys from file @var{filename} and append them to the
keyset @var{keys}.  See @code{shishi_key_print()} for the format of the input.

@strong{Return value:} Returns @code{SHISHI_OK} iff successful.

@strong{Since:} 0.0.42
@end deftypefun

@subheading shishi_keys_for_serverrealm_in_file
@anchor{shishi_keys_for_serverrealm_in_file}
@deftypefun {Shishi_key *} {shishi_keys_for_serverrealm_in_file} (@w{Shishi * @var{handle}}, @w{const char * @var{filename}}, @w{const char * @var{server}}, @w{const char * @var{realm}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{filename}: file to read keys from.
@*@var{server}: server name to get key for.
@*@var{realm}: realm of server to get key for.

@strong{Description:} Get keys that match specified @var{server} and @var{realm} from the key set
file @var{filename}.

@strong{Return value:} Returns the key for specific server and realm, read
from the indicated file, or NULL if no key could be found or an
error encountered.
@end deftypefun

@subheading shishi_keys_for_server_in_file
@anchor{shishi_keys_for_server_in_file}
@deftypefun {Shishi_key *} {shishi_keys_for_server_in_file} (@w{Shishi * @var{handle}}, @w{const char * @var{filename}}, @w{const char * @var{server}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{filename}: file to read keys from.
@*@var{server}: server name to get key for.

@strong{Description:} Get key for specified @var{server} from @var{filename}.

@strong{Return value:} Returns the key for specific server, read from the
indicated file, or NULL if no key could be found or an error
encountered.
@end deftypefun

@subheading shishi_keys_for_localservicerealm_in_file
@anchor{shishi_keys_for_localservicerealm_in_file}
@deftypefun {Shishi_key *} {shishi_keys_for_localservicerealm_in_file} (@w{Shishi * @var{handle}}, @w{const char * @var{filename}}, @w{const char * @var{service}}, @w{const char * @var{realm}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{filename}: file to read keys from.
@*@var{service}: service to get key for.
@*@var{realm}: realm of server to get key for, or NULL for default realm.

@strong{Description:} Get key for specified @var{service} and @var{realm} from @var{filename}.

@strong{Return value:} Returns the key for the server
"SERVICE/HOSTNAME@@REALM" (where HOSTNAME is the current system's
hostname), read from the default host keys file (see
@code{shishi_hostkeys_default_file()}), or NULL if no key could be found
or an error encountered.
@end deftypefun

