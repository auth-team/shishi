/* version.c --- Shishi version handling self tests.
 * Copyright (C) 2002-2022 Simon Josefsson
 *
 * This file is part of Shishi.
 *
 * Shishi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shishi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shishi; if not, see http://www.gnu.org/licenses or write
 * to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301, USA
 *
 */

#include "utils.c"

void
test (Shishi * handle)
{
  char *out = NULL;
  int i, j;
  int res;

  success ("Header version %s library version %s\n",
	   SHISHI_VERSION, shishi_check_version (NULL));

  if (!shishi_check_version (SHISHI_VERSION))
    fail ("shishi_check_version failure");

  if (!shishi_check_version ("1.0.1"))
    fail ("gsasl_check_version(1.0.1) failure");

  if (strcmp (SHISHI_VERSION, shishi_check_version (NULL)) != 0)
    fail ("header version mismatch library version\n");

  i = SHISHI_VERSION_MAJOR * 256 * 256 +
    SHISHI_VERSION_MINOR * 256 + SHISHI_VERSION_PATCH;

  j = asprintf (&out, "%d.%d.%d", SHISHI_VERSION_MAJOR,
		SHISHI_VERSION_MINOR, SHISHI_VERSION_PATCH);
  if (j <= 0)
    fail ("asprintf failure: %d", j);

  success ("Header version %s number %x derived %x out %s\n", out,
	   (unsigned) SHISHI_VERSION_NUMBER, (unsigned) i, out);

  if (SHISHI_VERSION_NUMBER != i)
    fail ("header version number mismatch\n");

  if (!shishi_check_version (out))
    fail ("gsasl_check_version(%s) failure\n", out);

  if (strncmp (SHISHI_VERSION, out, strlen (out)) != 0)
    fail ("header version numbers mismatch library version\n");

  free (out);
}
