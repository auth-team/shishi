@subheading shishi_key_principal
@anchor{shishi_key_principal}
@deftypefun {const char *} {shishi_key_principal} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get the principal part of the key owner principal name, i.e.,
except the realm.

@strong{Return value:} Returns the principal owning the key.  (Not a copy of
it, so don't modify or deallocate it.)
@end deftypefun

@subheading shishi_key_principal_set
@anchor{shishi_key_principal_set}
@deftypefun {void} {shishi_key_principal_set} (@w{Shishi_key * @var{key}}, @w{const char * @var{principal}})
@var{key}: structure that holds key information
@*@var{principal}: string with new principal name.

@strong{Description:} Set the principal owning the key. The string is copied into the
key, so you can dispose of the variable immediately after calling
this function.
@end deftypefun

@subheading shishi_key_realm
@anchor{shishi_key_realm}
@deftypefun {const char *} {shishi_key_realm} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get the realm part of the key owner principal name.

@strong{Return value:} Returns the realm for the principal owning the key.
(Not a copy of it, so don't modify or deallocate it.)
@end deftypefun

@subheading shishi_key_realm_set
@anchor{shishi_key_realm_set}
@deftypefun {void} {shishi_key_realm_set} (@w{Shishi_key * @var{key}}, @w{const char * @var{realm}})
@var{key}: structure that holds key information
@*@var{realm}: string with new realm name.

@strong{Description:} Set the realm for the principal owning the key. The string is
copied into the key, so you can dispose of the variable immediately
after calling this function.
@end deftypefun

@subheading shishi_key_type
@anchor{shishi_key_type}
@deftypefun {int} {shishi_key_type} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get key type.

@strong{Return value:} Returns the type of key as an integer as described in
the standard.
@end deftypefun

@subheading shishi_key_type_set
@anchor{shishi_key_type_set}
@deftypefun {void} {shishi_key_type_set} (@w{Shishi_key * @var{key}}, @w{int32_t @var{type}})
@var{key}: structure that holds key information
@*@var{type}: type to set in key.

@strong{Description:} Set the type of key in key structure.
@end deftypefun

@subheading shishi_key_value
@anchor{shishi_key_value}
@deftypefun {const char *} {shishi_key_value} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get the raw key bytes.

@strong{Return value:} Returns the key value as a pointer which is valid
throughout the lifetime of the key structure.
@end deftypefun

@subheading shishi_key_value_set
@anchor{shishi_key_value_set}
@deftypefun {void} {shishi_key_value_set} (@w{Shishi_key * @var{key}}, @w{const char * @var{value}})
@var{key}: structure that holds key information
@*@var{value}: input array with key data.

@strong{Description:} Set the key value and length in key structure.  The value is copied
into the key (in other words, you can deallocate @var{value} right after
calling this function without modifying the value inside the key).
@end deftypefun

@subheading shishi_key_version
@anchor{shishi_key_version}
@deftypefun {uint32_t} {shishi_key_version} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get the "kvno" (key version) of key.  It will be UINT32_MAX if the
key is not long-lived.

@strong{Return value:} Returns the version of key ("kvno").
@end deftypefun

@subheading shishi_key_version_set
@anchor{shishi_key_version_set}
@deftypefun {void} {shishi_key_version_set} (@w{Shishi_key * @var{key}}, @w{uint32_t @var{kvno}})
@var{key}: structure that holds key information
@*@var{kvno}: new version integer.

@strong{Description:} Set the version of key ("kvno") in key structure.  Use UINT32_MAX
for non-ptermanent keys.
@end deftypefun

@subheading shishi_key_timestamp
@anchor{shishi_key_timestamp}
@deftypefun {time_t} {shishi_key_timestamp} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Get the time the key was established.  Typically only present when
the key was imported from a keytab format.

@strong{Return value:} Returns the time the key was established, or
(time_t)-1 if not available.

@strong{Since:} 0.0.42
@end deftypefun

@subheading shishi_key_timestamp_set
@anchor{shishi_key_timestamp_set}
@deftypefun {void} {shishi_key_timestamp_set} (@w{Shishi_key * @var{key}}, @w{time_t @var{timestamp}})
@var{key}: structure that holds key information
@*@var{timestamp}: new timestamp.

@strong{Description:} Set the time the key was established.  Typically only relevant when
exporting the key to keytab format.

@strong{Since:} 0.0.42
@end deftypefun

@subheading shishi_key_name
@anchor{shishi_key_name}
@deftypefun {const char *} {shishi_key_name} (@w{Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Calls shishi_cipher_name for key type.

@strong{Return value:} Return name of key.
@end deftypefun

@subheading shishi_key_length
@anchor{shishi_key_length}
@deftypefun {size_t} {shishi_key_length} (@w{const Shishi_key * @var{key}})
@var{key}: structure that holds key information

@strong{Description:} Calls shishi_cipher_keylen for key type.

@strong{Return value:} Returns the length of the key value.
@end deftypefun

@subheading shishi_key
@anchor{shishi_key}
@deftypefun {int} {shishi_key} (@w{Shishi * @var{handle}}, @w{Shishi_key ** @var{key}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{key}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

@subheading shishi_key_done
@anchor{shishi_key_done}
@deftypefun {void} {shishi_key_done} (@w{Shishi_key * @var{key}})
@var{key}: pointer to structure that holds key information.

@strong{Description:} Deallocates key information structure.
@end deftypefun

@subheading shishi_key_copy
@anchor{shishi_key_copy}
@deftypefun {void} {shishi_key_copy} (@w{Shishi_key * @var{dstkey}}, @w{Shishi_key * @var{srckey}})
@var{dstkey}: structure that holds destination key information
@*@var{srckey}: structure that holds source key information

@strong{Description:} Copies source key into existing allocated destination key.
@end deftypefun

@subheading shishi_key_from_value
@anchor{shishi_key_from_value}
@deftypefun {int} {shishi_key_from_value} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{const char * @var{value}}, @w{Shishi_key ** @var{key}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{value}: input array with key value, or NULL.
@*@var{key}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure, and set the key type and
key value. KEY contains a newly allocated structure only if this
function is successful.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

@subheading shishi_key_from_base64
@anchor{shishi_key_from_base64}
@deftypefun {int} {shishi_key_from_base64} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{const char * @var{value}}, @w{Shishi_key ** @var{key}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{value}: input string with base64 encoded key value, or NULL.
@*@var{key}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure, and set the key type and
key value. KEY contains a newly allocated structure only if this
function is successful.

@strong{Return value:} Returns SHISHI_INVALID_KEY if the base64 encoded key
length doesn't match the key type, and SHISHI_OK on
success.
@end deftypefun

@subheading shishi_key_random
@anchor{shishi_key_random}
@deftypefun {int} {shishi_key_random} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{Shishi_key ** @var{key}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{key}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure for the key type and some
random data.  KEY contains a newly allocated structure only if this
function is successful.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

@subheading shishi_key_from_random
@anchor{shishi_key_from_random}
@deftypefun {int} {shishi_key_from_random} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{const char * @var{rnd}}, @w{size_t @var{rndlen}}, @w{Shishi_key ** @var{outkey}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{rnd}: random data.
@*@var{rndlen}: length of random data.
@*@var{outkey}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure, and set the key type and
key value using @code{shishi_random_to_key()}.  KEY contains a newly
allocated structure only if this function is successful.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

@subheading shishi_key_from_string
@anchor{shishi_key_from_string}
@deftypefun {int} {shishi_key_from_string} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{const char * @var{password}}, @w{size_t @var{passwordlen}}, @w{const char * @var{salt}}, @w{size_t @var{saltlen}}, @w{const char * @var{parameter}}, @w{Shishi_key ** @var{outkey}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{password}: input array containing password.
@*@var{passwordlen}: length of input array containing password.
@*@var{salt}: input array containing salt.
@*@var{saltlen}: length of input array containing salt.
@*@var{parameter}: input array with opaque encryption type specific information.
@*@var{outkey}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure, and set the key type and
key value using @code{shishi_string_to_key()}.  KEY contains a newly
allocated structure only if this function is successful.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

@subheading shishi_key_from_name
@anchor{shishi_key_from_name}
@deftypefun {int} {shishi_key_from_name} (@w{Shishi * @var{handle}}, @w{int32_t @var{type}}, @w{const char * @var{name}}, @w{const char * @var{password}}, @w{size_t @var{passwordlen}}, @w{const char * @var{parameter}}, @w{Shishi_key ** @var{outkey}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{type}: type of key.
@*@var{name}: principal name of user.
@*@var{password}: input array containing password.
@*@var{passwordlen}: length of input array containing password.
@*@var{parameter}: input array with opaque encryption type specific information.
@*@var{outkey}: pointer to structure that will hold newly created key information

@strong{Description:} Create a new Key information structure, and derive the key from
principal name and password using @code{shishi_key_from_name()}.  The salt
is derived from the principal name by concatenating the decoded
realm and principal.

@strong{Return value:} Returns SHISHI_OK iff successful.
@end deftypefun

