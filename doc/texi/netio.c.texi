@subheading shishi_kdc_sendrecv_hint
@anchor{shishi_kdc_sendrecv_hint}
@deftypefun {int} {shishi_kdc_sendrecv_hint} (@w{Shishi * @var{handle}}, @w{const char * @var{realm}}, @w{const char * @var{indata}}, @w{size_t @var{inlen}}, @w{char ** @var{outdata}}, @w{size_t * @var{outlen}}, @w{Shishi_tkts_hint * @var{hint}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{realm}: string with realm name.
@*@var{indata}: Packet to send to KDC.
@*@var{inlen}: Length of @var{indata}.
@*@var{outdata}: Newly allocated string with data returned from KDC.
@*@var{outlen}: Length of @var{outdata}.
@*@var{hint}: a @code{Shishi_tkts_hint} structure with flags.

@strong{Description:} Send packet to KDC for realm and receive response.  The code finds
KDC addresses from configuration file, then by querying for SRV
records for the realm, and finally by using the realm name as a
hostname.

@strong{Returns:} @code{SHISHI_OK} on success, @code{SHISHI_KDC_TIMEOUT} if a timeout
was reached, or other errors.
@end deftypefun

@subheading shishi_kdc_sendrecv
@anchor{shishi_kdc_sendrecv}
@deftypefun {int} {shishi_kdc_sendrecv} (@w{Shishi * @var{handle}}, @w{const char * @var{realm}}, @w{const char * @var{indata}}, @w{size_t @var{inlen}}, @w{char ** @var{outdata}}, @w{size_t * @var{outlen}})
@var{handle}: Shishi library handle create by @code{shishi_init()}.
@*@var{realm}: string with realm name.
@*@var{indata}: Packet to send to KDC.
@*@var{inlen}: Length of @var{indata}.
@*@var{outdata}: Newly allocated string with data returned from KDC.
@*@var{outlen}: Length of @var{outdata}.

@strong{Description:} Send packet to KDC for realm and receive response.  The code finds
KDC addresses from configuration file, then by querying for SRV
records for the realm, and finally by using the realm name as a
hostname.

@strong{Returns:} @code{SHISHI_OK} on success, @code{SHISHI_KDC_TIMEOUT} if a timeout
was reached, or other errors.
@end deftypefun

