@subheading shishi_check_version
@anchor{shishi_check_version}
@deftypefun {const char *} {shishi_check_version} (@w{const char * @var{req_version}})
@var{req_version}: Oldest acceptable version, or @code{NULL}.

@strong{Description:} Checks that the installed library version is at least
as recent as the one provided in @var{req_version}.
The version string is formatted like "1.0.2".

Whenever @code{NULL} is passed to this function, the check is
suppressed, but the library version is still returned.

@strong{Return value:} Returns the active library version,
or @code{NULL}, should the running library be too old.
@end deftypefun

